package component

import (
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/jakecoffman/cp"
)

type Input struct{}

func NewInput(object *cp.Shape) *Input {
	return &Input{}
}

func (i *Input) Move() string {
	switch true {
	case ebiten.IsKeyPressed(ebiten.KeyLeft):
		return "left"
	case ebiten.IsKeyPressed(ebiten.KeyRight):
		return "right"
	case ebiten.IsKeyPressed(ebiten.KeySpace):
		return "jump"
	default:
		return "none"
	}
}

func (i *Input) Update() {
	if ebiten.IsMouseButtonPressed(ebiten.MouseButtonLeft) {
		log.Println("Mouse button pressed")
	}
}
