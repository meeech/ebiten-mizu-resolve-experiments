package component

import (
	"image/color"
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/jakecoffman/cp"
	"golang.org/x/image/colornames"
)

type Renderer struct {
	C *color.RGBA
}

// So this is analogous to the sprite
func (r Renderer) Draw(screen *ebiten.Image, p *Physics) {
	shape := p.Object
	body := shape.Body()
	pos := body.Position()

	if r.C == nil {
		r.C = &colornames.Red
	}

	switch shape.Class.(type) {
	case *cp.Circle:
		circle := shape.Class.(*cp.Circle)
		ebitenutil.DrawCircle(screen, pos.X, pos.Y, circle.Radius(), r.C)
	case *cp.Segment:
		log.Println("segment")
	case *cp.PolyShape:
		bb := cp.ShapeGetBB(shape)
		w := bb.R - bb.L
		h := bb.T - bb.B
		ebitenutil.DrawRect(screen, pos.X, pos.Y, w, h, r.C)
	default:
		log.Println("unknown")
	}
}

func NewRenderer(c *color.RGBA) Renderer {
	return Renderer{C: c}
}
