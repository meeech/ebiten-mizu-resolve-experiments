package main

import (
	"log"

	ebiten "github.com/hajimehoshi/ebiten/v2"
	"github.com/jakecoffman/cp"
	"github.com/meeech/ex-5/component"
	"github.com/meeech/ex-5/entity"
	"github.com/meeech/ex-5/system"
	"github.com/sedyh/mizu/pkg/engine"
)

var space *cp.Space

type Game struct{}

// Main scene, you can use that for settings or main menu
func (g *Game) Setup(w engine.World) {
	bounds := w.Bounds()
	space = cp.NewSpace()
	space.Iterations = 5
	space.SetGravity(cp.Vector{X: 0, Y: 300})

	w.AddComponents(
		component.Input{},
		component.Warden{},
		component.Physics{},
		component.Renderer{},
	)

	w.AddEntities(entity.NewDisc(space, float64(bounds.Dx())/2, 20, 20))
	w.AddEntities(entity.NewPlayer(space, float64(bounds.Dx())/2, 20, 20))

	// 3 walls make a room.
	w.AddEntities(entity.NewWall(space, -5, 0, 20.0, float64(bounds.Dy())))                      // left wall
	w.AddEntities(entity.NewWall(space, float64(bounds.Dx())-15, 0, 20.0, float64(bounds.Dy()))) // right wall
	w.AddEntities(entity.NewWall(space, 0, float64(bounds.Dy())-15.0, float64(bounds.Dx()), 20.0))
	w.AddEntities(entity.NewWall(space, 0, 0, float64(bounds.Dx()), 20.0))

	w.AddSystems(
		system.NewSpace(space),
		&system.Render{},
	)
}

func main() {
	ebiten.SetWindowResizable(false)
	ebiten.SetFPSMode(ebiten.FPSModeVsyncOffMaximum)
	ebiten.SetWindowSize(640, 480)
	if err := ebiten.RunGame(engine.NewGame(&Game{})); err != nil {
		log.Fatal(err)
	}
}
