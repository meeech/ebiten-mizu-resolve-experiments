# ex 5

* x room
* x box walking around
* x jump

Problem: Friction doesn't seem to take over when disc stops bouncing. the disc then just glides along the surface, hits a wall, bounces back and vel stays constant. I am expecting friction to bleed vel off of the disc. When disc bounces off of side walls, then it loses speed. What am I doing wrong?
A: Nothing. c2d doesnt bleed off vel in that scenario. Needed to handle it manually


# ex 4

re-implement ex 3 with CP

* almost there. Just need to figure out where the reposition/reset code should go - who should be responsible for that? like, i would expect the 'entity' to be responsible, but maybe it just needs a 'warden' component - it examines its position, and teleports it if it is out of bounds. yes, that sounds like the solution? a warden component and a jail system?


Plinko game

# ex 3

Plinko game

x bounce around off posts
    x debugging bounding boxes... so lets take a step back.

- click mouse to drop disc - will prolly drop this requirement.
- make peg change colour on collision?
    - need a colliding flag we can examine in renderer
    -
physics far from perfect here, but its got some good bounce to it.

@bookmark - can try to reduce bounding box by a few pixels - maybe that will help the illusion?
maybe next experiment work with chipmunk physics?

maybe need to try the intersection method?


# ex 2

We have something on screen.

x Refactor

x scope change: Use resolv https://github.com/SolarLune/resolv

x Get gravity working

x Hit a floor to stop falling.
---
Ok.

Velocity as example

system.Velocity, in the type struct, I tell it This system (velocity) is connected to anything with a component.Pos and component.Vel
the `func (v Velocity) Update(...)` method on system.Velocity is then called once for each Entity which has these coponents. these components are passed to me via the system.Velocity type struct. :facepalm: i think i get it a bit more.
---

Trouble wrapping head around - how far do yo ugo in decomposing? Since then the order seems to matter more and more and am going to make a tangled mess?


# ex 1

OK

main

leads to mizu.NewGame

Game needs setup

Setup attach all systems. So a scene (for example) doesn't have Draw method on it. Rather we add a render system, and the Draw is called there.

Building on this, we have 2 components added - Pos & Rad
Then we add an entity that uses pos and rad

which then lets us query the world for all items with a Pos and a Rad,
 `view := w.View(Pos{}, Rad{})`
whicj we can then ace on in the Draw method of renderer

So now, we add velocity - same idea, except we make something to handle velocy explicitly. It has an Update method on it, so this System will be applied automatically to every entity with those components attached - we dont have to do the view.Each like in Draw
As long as we tremember to register the System

goal


YES * get square on screen moving using system?
YES * basic game loop
YES * understanding ecs


