package entity

import (
	"math/rand"

	"github.com/jakecoffman/cp"
	"github.com/meeech/ex-5/component"
	"golang.org/x/image/colornames"
)

type Disc struct {
	// Warden component.Warden
	Ren component.Renderer
	Obj component.Physics
}

func NewDisc(space *cp.Space, x, y, rad float64) *Disc {
	mass := 1.0

	disc := cp.NewBody(mass, cp.MomentForCircle(mass, 0, rad, cp.Vector{}))
	disc = space.AddBody(disc)
	disc.SetPosition(cp.Vector{X: x, Y: y})
	shape := cp.NewCircle(disc, rad, cp.Vector{})
	shape.SetElasticity(0.5)
	shape.SetFriction(1)
	shape = space.AddShape(shape)
	disc.SetVelocity(float64(rand.Intn(640)-320), 0)
	_disc := &Disc{
		// Warden: *component.NewWarden(),
		Ren: component.NewRenderer(&colornames.Black),
		Obj: *component.NewPhysics(shape),
	}
	return _disc
}
