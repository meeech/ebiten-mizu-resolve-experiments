package entity

import (
	"github.com/jakecoffman/cp"
	"github.com/meeech/ex-5/component"
)

type Peg struct {
	Ren component.Renderer
	Obj component.Physics
	// Debug component.Debug
}

func NewPeg(space *cp.Space, x, y, rad float64) *Peg {
	// diameter := rad * 2

	body := cp.NewStaticBody()
	body = space.AddBody(body)
	body.SetPosition(cp.Vector{X: x, Y: y})
	shape := cp.NewCircle(body, rad, cp.Vector{})
	shape.SetElasticity(1)
	shape.SetFriction(1)
	shape = space.AddShape(shape)
	return &Peg{
		Ren: component.NewRenderer(nil),
		Obj: *component.NewPhysics(shape),
		// Debug: component.Debug{},
	}
}
