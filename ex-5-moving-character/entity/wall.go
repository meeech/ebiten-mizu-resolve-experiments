package entity

import (
	"github.com/jakecoffman/cp"
	"github.com/meeech/ex-5/component"
)

type Wall struct {
	Ren component.Renderer
	Obj component.Physics
	// Debug component.Debug
}

func NewWall(space *cp.Space, x, y, w, h float64) *Wall {
	body := cp.NewStaticBody()
	body = space.AddBody(body)
	body.SetPosition(cp.Vector{X: x, Y: y})

	shape := NewBox(body, w, h, 0)
	shape.SetElasticity(1)
	shape.SetFriction(1)
	shape = space.AddShape(shape)
	return &Wall{
		Ren: component.NewRenderer(nil),
		Obj: *component.NewPhysics(shape),
		// Debug: component.Debug{},
	}
}

func NewBox(body *cp.Body, w, h, r float64) *cp.Shape {
	bb := &cp.BB{L: 0, B: 0, R: w, T: h}
	verts := []cp.Vector{
		{bb.R, bb.B},
		{bb.R, bb.T},
		{bb.L, bb.T},
		{bb.L, bb.B},
	}
	return cp.NewPolyShapeRaw(body, 4, verts, r)
}
