package entity

import (
	"github.com/jakecoffman/cp"
	"github.com/meeech/ex-5/component"
	"golang.org/x/image/colornames"
)

type Player struct {
	// Warden component.Warden
	Input component.Input
	Ren   component.Renderer
	Obj   component.Physics
}

func NewPlayer(space *cp.Space, x, y, rad float64) *Player {
	playerBody := space.AddBody(cp.NewBody(10, cp.INFINITY))
	playerBody.SetPosition(cp.Vector{X: 100, Y: 200})

	playerShape := space.AddShape(NewBox(playerBody, 20, 100, 0))
	playerShape.SetElasticity(0.75)
	playerShape.SetFriction(0)
	playerShape.SetCollisionType(1)

	_player := &Player{
		Ren: component.NewRenderer(&colornames.Green),
		Obj: *component.NewPhysics(playerShape),
	}
	return _player
}
