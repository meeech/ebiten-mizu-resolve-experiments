package system

import (
	"log"
	"math/rand"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/jakecoffman/cp"
	"github.com/meeech/ex-5/component"
	"github.com/sedyh/mizu/pkg/engine"
)

type Space struct {
	space *cp.Space
}

func NewSpace(space *cp.Space) *Space {
	return &Space{
		space: space,
	}
}

func (s *Space) Draw(w engine.World, screen *ebiten.Image) {
	// log.Println("system.Space.Draw")
}

func (s *Space) Update(w engine.World) {
	inputEnts := w.View(component.Input{}, component.Physics{})
	inputEnts.Each(func(e engine.Entity) {
		var p *component.Physics
		var i *component.Input
		e.Get(&p, &i)

		deltaX, deltaY := 0.0, 0.0
		log.Println(i.Move())
		switch i.Move() {
		case "left":
			deltaX = -10
		case "right":
			deltaX = 10
		case "jump":
			deltaY = -100
		}

		p.Object.Body().SetForce(cp.Vector{X: deltaX * 100, Y: deltaY * 100})
	})

	pEnts := w.View(component.Physics{})
	pEnts.Each(func(e engine.Entity) {
		var p *component.Physics
		e.Get(&p)
		body := p.Object.Body()
		// rolling friction https://chipmunk-physics.net/forum/viewtopic.php?t=536
		body.EachArbiter(func(arb *cp.Arbiter) {
			// log.Printf("Collision!")
			body.SetAngularVelocity(body.AngularVelocity() * 0.95)
			// n := arb.Normal()
			// n.Y == 1 collision is the bottom of the disc
			// grounded = n.Y > 0
		})
	})

	// Anything with a warden tells us we need to restrict it to a certain area
	ents := w.View(component.Physics{}, component.Warden{})
	// log.Printf("space:update %-v", ents)
	ents.Each(func(e engine.Entity) {
		var p *component.Physics
		grounded := false

		e.Get(&p)
		body := p.Object.Body()
		body.EachArbiter(func(arb *cp.Arbiter) {
			// log.Printf("Collision!")
			n := arb.Normal()
			// n.Y == 1 collision is the bottom of the disc
			grounded = n.Y > 0
		})

		pos := body.Position()
		vel := body.Velocity()
		log.Printf("%+v", vel)
		escaped := (pos.X < 0 || pos.X > 640 || pos.Y > 490)
		if grounded || escaped {
			log.Printf("grounded: %+v escaped: %+v", grounded, escaped)
			body.SetPosition(cp.Vector{X: float64(rand.Intn(640)), Y: 0})
			body.SetVelocity(float64(rand.Intn(640)-320), 0)
		}
	})

	// log.Println("system.Space.Update")
	// MaxTPS replaced by TPS in 2.4
	s.space.Step(1.0 / float64(ebiten.MaxTPS()))
}
