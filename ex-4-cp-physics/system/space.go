package system

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/jakecoffman/cp"
	"github.com/meeech/ex-4/component"
	"github.com/sedyh/mizu/pkg/engine"
)

type Space struct {
	space *cp.Space
}

func NewSpace(space *cp.Space) *Space {
	return &Space{
		space: space,
	}
}

func (s *Space) Draw(w engine.World, screen *ebiten.Image) {
	// log.Println("system.Space.Draw")
}

func (s *Space) Update(w engine.World) {
	// Anything with a warden tells us
	ents := w.View(component.Physics{}, component.Warden{})
	ents.Each(func(e engine.Entity) {
		var p *component.Physics
		e.Get(&p)
		pos := p.Object.Body().Position()
		if pos.X < 0 || pos.X > 480 || pos.Y > 640 {
			p.Object.Body().SetPosition(cp.Vector{X: 320, Y: 0})
		}
	})

	// log.Println("system.Space.Update")
	// MaxTPS replaced by TPS in 2.4
	s.space.Step(1.0 / float64(ebiten.MaxTPS()))
}
