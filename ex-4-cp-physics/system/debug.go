package system

// import (
// 	"github.com/hajimehoshi/ebiten/v2"
// 	"github.com/meeech/ex-4/component"
// 	"github.com/sedyh/mizu/pkg/engine"
// )

// func NewDebug(active bool) *Debug {
// 	return &Debug{
// 		active: active,
// 	}
// }

// // When you need many sets of components
// // in one system, you can use the views
// // To expand, what is it saying, usually you 'bind' a system to a component
// type Debug struct {
// 	active bool
// }

// // Render one frame
// func (d *Debug) Draw(w engine.World, screen *ebiten.Image) {
// 	if d.active == false {
// 		return
// 	}
// 	ents := w.View(component.Debug{})
// 	ents.Each(func(e engine.Entity) {
// 		// for now, assume anything with a renderer has a position
// 		var dim *component.Dim
// 		var pos *component.Pos
// 		var rad *component.Rad
// 		var debug *component.Debug
// 		var phys *component.Physics
// 		e.Get(&pos, &debug, &rad, &dim, &phys)
// 		// draw the component
// 		debug.Draw(screen, pos, rad, dim, phys)
// 	})
// }
