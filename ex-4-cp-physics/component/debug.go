package component

// import (
// 	"log"

// 	"github.com/hajimehoshi/ebiten/v2"
// 	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
// 	"golang.org/x/image/colornames"
// )

// var wireframeColor = colornames.Green

// type Debug struct{}

// // So this is analogous to the sprite
// func (d Debug) Draw(screen *ebiten.Image, pos *Pos, rad *Rad, dim *Dim, phys *Physics) {
// 	pObj := phys.Object
// 	switch true {
// 	case dim != nil:
// 		// ebitenutil.DrawRect(
// 		// 	screen, pos.X, pos.Y, dim.W, dim.H, wireframeColor,
// 		// )
// 		ebitenutil.DrawRect(
// 			screen, pObj.X, pObj.Y, pObj.W, pObj.H, colornames.Yellow,
// 		)

// 	case rad != nil:
// 		// yellow == physical resolv OBJECT
// 		// so for circles i need to fix the phys obj
// 		ebitenutil.DrawRect(
// 			screen, pObj.X, pObj.Y, pObj.W, pObj.H, colornames.Yellow,
// 		)

// 		// green == the drawn box. Not too painful.
// 		ebitenutil.DrawLine(screen, pos.X-rad.Value, pos.Y-rad.Value, pos.X+rad.Value, pos.Y-rad.Value, wireframeColor)
// 		ebitenutil.DrawLine(screen, pos.X+rad.Value, pos.Y-rad.Value, pos.X+rad.Value, pos.Y+rad.Value, wireframeColor)
// 		ebitenutil.DrawLine(screen, pos.X+rad.Value, pos.Y+rad.Value, pos.X-rad.Value, pos.Y+rad.Value, wireframeColor)
// 		ebitenutil.DrawLine(screen, pos.X-rad.Value, pos.Y-rad.Value, pos.X-rad.Value, pos.Y+rad.Value, wireframeColor)
// 	default:
// 		log.Println("component.Renderer.Draw: no Dim or Rad")
// 	}
// }
