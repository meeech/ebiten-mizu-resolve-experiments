package component

import (
	"github.com/jakecoffman/cp"
)

type Physics struct {
	Object *cp.Shape
}

func NewPhysics(object *cp.Shape) *Physics {
	return &Physics{
		Object: object,
	}
}
