package main

import (
	"log"

	ebiten "github.com/hajimehoshi/ebiten/v2"
	"github.com/jakecoffman/cp"
	"github.com/meeech/ex-4/component"
	"github.com/meeech/ex-4/entity"
	"github.com/meeech/ex-4/system"
	"github.com/sedyh/mizu/pkg/engine"
)

func main() {
	ebiten.SetWindowResizable(true)
	ebiten.SetFPSMode(ebiten.FPSModeVsyncOffMaximum)
	ebiten.SetWindowSize(480, 640)
	if err := ebiten.RunGame(engine.NewGame(&Game{})); err != nil {
		log.Fatal(err)
	}
}

var space *cp.Space

type Game struct{}

// Main scene, you can use that for settings or main menu
func (g *Game) Setup(w engine.World) {
	bounds := w.Bounds()

	space = cp.NewSpace()
	space.Iterations = 5
	space.SetGravity(cp.Vector{X: 0, Y: 100})

	w.AddComponents(
		component.Warden{},
		component.Physics{},
		component.Renderer{},
	)

	for i := 0; i < 8; i++ {
		yOffset := i * 80
		xOffset := 20
		if i%2 == 0 {
			xOffset = -xOffset
		}
		for j := 1; j < 6; j++ {
			x, y := float64((j*80)+xOffset), float64(50+yOffset)
			w.AddEntities(entity.NewPeg(space, x, y, 15.0))
		}
	}

	w.AddEntities(entity.NewDisc(space, float64(bounds.Dx())/2, 20, 20))

	w.AddSystems(
		system.NewSpace(space),
		&system.Render{},
	)
}
