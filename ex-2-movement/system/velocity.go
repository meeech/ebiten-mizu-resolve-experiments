package system

import (
	"log"

	"github.com/meeech/ex-2/component"
	"github.com/sedyh/mizu/pkg/engine"
)

type Velocity struct{}

// We need to handle some different cases here for collisions.
// So now, we will manually get the entitys / components we need.
func (v *Velocity) Update(w engine.World) {
	// Get anything with a pos, vel, and physical object
	log.Println("system.Velocity.Update()")
	w.View(
		component.Pos{},
		component.Vel{},
		component.Physics{},
	).Each(func(e engine.Entity) {
		var pos *component.Pos
		var vel *component.Vel
		var phys *component.Physics
		e.Get(&pos, &vel, &phys)

		if collision := phys.Object.Check(vel.L, vel.M); collision != nil {
			log.Printf("COLLLLINSIOSNS!!!!***** %+v ", collision)
			vel.L = collision.ContactWithObject(collision.Objects[0]).X()
			vel.M = collision.ContactWithObject(collision.Objects[0]).Y()
		}
		pos.X += vel.L
		pos.Y += vel.M
		log.Printf("system.Velocity.Update %+v", pos)
	})
}
