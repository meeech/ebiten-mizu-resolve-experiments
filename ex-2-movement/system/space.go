package system

import (
	"log"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/meeech/ex-2/component"
	"github.com/sedyh/mizu/pkg/engine"
	"github.com/solarlune/resolv"
)

type Space struct {
	space *resolv.Space
	*component.Physics
	*component.Pos
}

func NewSpace(space *resolv.Space) *Space {
	return &Space{
		space: space,
	}
}

func (s *Space) Draw(w engine.World, screen *ebiten.Image) {
	// log.Println("system.Space.Draw")
}

func (s *Space) Update(w engine.World) {
	s.Physics.Object.X = s.Pos.X
	s.Physics.Object.Y = s.Pos.Y
	log.Printf("system.Space.Update %+v", s.Pos)
	s.Physics.Object.Update()
	log.Printf("%+v", s.space.Objects())
}
