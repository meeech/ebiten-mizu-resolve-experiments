package entity

import (
	"github.com/meeech/ex-2/component"
)

type Ball struct {
	Pos component.Pos
	Rad component.Rad
	Vel component.Vel
	Ren component.Renderer
	Obj component.Physics
}
