package component

import (
	"image"
	"image/color"
	"log"
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"golang.org/x/image/colornames"
)

type Renderer struct{}

// So this is analogous to the sprite
func (r Renderer) Draw(screen *ebiten.Image, pos *Pos, rad *Rad, dim *Dim) {
	switch true {
	case dim != nil:
		ebitenutil.DrawRect(
			screen, pos.X, pos.Y, dim.W, dim.H, colornames.Darkred,
		)
	case rad != nil:
		DrawCircle(screen, pos.X, pos.Y, rad.Value, colornames.Red)
	default:
		log.Println("component.Renderer.Draw: no Dim or Rad")
	}
}

func NewRenderer() Renderer {
	return Renderer{}
}

// Coming in the next version of ebiten
var (
	emptyImage    = ebiten.NewImage(3, 3)
	emptySubImage = emptyImage.SubImage(image.Rect(1, 1, 2, 2)).(*ebiten.Image)
)

func DrawCircle(dst *ebiten.Image, cx, cy, r float64, clr color.Color) {
	var path vector.Path
	rd, g, b, a := clr.RGBA()

	emptyImage.Fill(color.White)

	path.Arc(float32(cx), float32(cy), float32(r), 0, 2*math.Pi, vector.Clockwise)

	verticles, indices := path.AppendVerticesAndIndicesForFilling(nil, nil)
	for i := range verticles {
		verticles[i].SrcX = 1
		verticles[i].SrcY = 1
		verticles[i].ColorR = float32(rd) / 0xffff
		verticles[i].ColorG = float32(g) / 0xffff
		verticles[i].ColorB = float32(b) / 0xffff
		verticles[i].ColorA = float32(a) / 0xffff
	}
	dst.DrawTriangles(verticles, indices, emptySubImage, nil)
}
