package component

import "github.com/solarlune/resolv"

type Physics struct {
	Object *resolv.Object
}

func NewPhysics(object *resolv.Object) *Physics {
	return &Physics{
		Object: object,
	}
}
