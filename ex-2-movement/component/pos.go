package component

// Position for any entity, if it needs
type Pos struct {
	X, Y float64 // Just a 2D point
}
