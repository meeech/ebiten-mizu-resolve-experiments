package main

import (
	"log"

	ebiten "github.com/hajimehoshi/ebiten/v2"
	"github.com/meeech/ex-2/component"
	"github.com/meeech/ex-2/entity"
	"github.com/meeech/ex-2/system"
	"github.com/sedyh/mizu/pkg/engine"
	"github.com/solarlune/resolv"
)

func main() {
	ebiten.SetWindowResizable(true)
	ebiten.SetFPSMode(ebiten.FPSModeVsyncOffMaximum)

	if err := ebiten.RunGame(engine.NewGame(&Game{})); err != nil {
		log.Fatal(err)
	}
}

var space *resolv.Space

type Game struct{}

// Main scene, you can use that for settings or main menu
func (g *Game) Setup(w engine.World) {
	bounds := w.Bounds()
	space = resolv.NewSpace(bounds.Dx(), bounds.Dy(), 16, 16)
	log.Printf("Setup %+v", bounds.Dx(), bounds.Dy())
	w.AddComponents(
		component.Dim{},
		component.Physics{},
		component.Pos{},
		component.Rad{},
		component.Renderer{},
		component.Vel{},
	)

	pos := component.Pos{
		X: float64(bounds.Dx() / 2),
		Y: float64(bounds.Dy() / 2),
	}
	w.AddEntities(
		&entity.Ball{
			Pos: pos,
			Rad: component.Rad{30},
			// Vel: component.Vel{0, 0},
			Ren: component.NewRenderer(),
			Obj: *component.NewPhysics(resolv.NewObject(pos.X, pos.Y, 30, 30)),
		},
		&entity.Ball{
			Pos: component.Pos{100, 100},
			Rad: component.Rad{120},
			Ren: component.NewRenderer(),
			Vel: component.Vel{0, 0},
			Obj: *component.NewPhysics(resolv.NewObject(100, 100, 120, 120)),
		},
		entity.NewWall(
			component.NewRenderer(),
			component.Dim{float64(bounds.Dx()), 30.0},
			component.Pos{0, float64(bounds.Dy() - 50)},
			*component.NewPhysics(resolv.NewObject(0, float64(bounds.Dy()-50), float64(bounds.Dx()), 30.0)),
		),
	)

	// Set up the physical world - dimensions
	w.View(component.Dim{}, component.Physics{}).Each(func(e engine.Entity) {
		var dim *component.Dim
		var pos *component.Pos
		var physics *component.Physics
		e.Get(&pos, &dim, &physics)
		//@todo need to modify the OBJECT shape too - ightnow it 0,0
		physics.Object.W = dim.W
		physics.Object.H = dim.H
		physics.Object.SetShape(resolv.NewRectangle(0, 0, dim.W, dim.H))
		space.Add(physics.Object)
		physics.Object.Update()
	})

	w.View(component.Rad{}, component.Physics{}).Each(func(e engine.Entity) {
		var rad *component.Rad
		var pos *component.Pos
		var physics *component.Physics
		e.Get(&pos, &rad, &physics)
		physics.Object.W = rad.Value
		physics.Object.H = rad.Value
		physics.Object.SetShape(resolv.NewCircle(pos.X, pos.Y, rad.Value))
		space.Add(physics.Object)
		physics.Object.Update()
	})

	// space.Add(
	// 	resolv.NewObject(pos.X, pos.Y, 30, 30),
	// )
	// playerObj = resolv.NewObject(32, 32, 16, 16)

	// Order affects when things are called.
	// assuming
	// w.AddSystems(
	// 	&system.Velocity{},
	// 	&system.Render{},
	// 	system.Space{},
	// )
	// calls:
	// system.Velocity.Update()
	// system.Render.Update()
	// system.Space.Update()
	// system.Velocity.Draw()
	// system.Render.Draw()
	// system.Space.Draw()

	w.AddSystems(
		&system.Gravity{},
		&system.Velocity{},
		system.NewSpace(space),
		&system.Render{},
	)
}

// Not sure where layout goes to???
func (g *Game) Layout(w, h int) (int, int) {
	return w, h
}
