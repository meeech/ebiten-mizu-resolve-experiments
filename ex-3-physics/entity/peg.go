package entity

import (
	"github.com/meeech/ex-3/component"
	"github.com/solarlune/resolv"
)

type Peg struct {
	Rad   component.Rad
	Ren   component.Renderer
	Pos   component.Pos
	Obj   component.Physics
	Debug component.Debug
}

func NewPeg(space *resolv.Space, x, y, rad float64) *Peg {
	// circles are drawn from x/y center (maybe I should just change that?)
	diameter := rad * 2

	physicalPeg := resolv.NewObject(x-rad, y-rad, diameter, diameter)
	physicalPeg.SetShape(resolv.NewCircle(x-rad, y-rad, rad))
	space.Add(physicalPeg)
	physicalPeg.Update()
	// for real I dont understand this. the object i'm storing has X 45, but when i read it later its back to 70???
	return &Peg{
		Rad:   component.Rad{Value: rad},
		Ren:   component.NewRenderer(nil),
		Pos:   component.Pos{X: x, Y: y}, // center of the circle
		Obj:   *component.NewPhysics(physicalPeg),
		Debug: component.Debug{},
	}
}
