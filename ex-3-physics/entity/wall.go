package entity

import (
	"github.com/meeech/ex-3/component"
)

// Your game object
type Wall struct {
	Dim component.Dim
	Ren component.Renderer
	Pos component.Pos
	Obj component.Physics
}

func NewWall(ren component.Renderer, dim component.Dim, pos component.Pos, obj component.Physics) *Wall {
	return &Wall{
		Dim: dim,
		Ren: ren,
		Pos: pos,
		Obj: obj,
	}
}
