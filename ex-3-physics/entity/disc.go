package entity

import (
	"github.com/meeech/ex-3/component"
	"github.com/solarlune/resolv"
	"golang.org/x/image/colornames"
)

type Disc struct {
	Rad component.Rad
	Ren component.Renderer
	Pos component.Pos
	Vel component.Vel
	Obj component.Physics
}

func NewDisc(space *resolv.Space, x, y, rad float64) *Disc {
	physicalDisc := resolv.NewObject(x, y, rad, rad)
	physicalDisc.SetShape(resolv.NewCircle(x, y, rad/2))
	space.Add(physicalDisc)
	physicalDisc.Update()

	disc := &Disc{
		Rad: component.Rad{Value: rad},
		Ren: component.NewRenderer(&colornames.Black),
		Pos: component.Pos{X: x, Y: y},
		Obj: *component.NewPhysics(physicalDisc),
	}
	return disc
}
