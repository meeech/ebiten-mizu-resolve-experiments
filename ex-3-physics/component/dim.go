package component

type Dim struct {
	W, H float64 //
}

func NewDim(w, h float64) *Dim {
	return &Dim{W: w, H: h}
}
