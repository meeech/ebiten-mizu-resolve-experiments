package component

type Vel struct {
	// L is the left/right velocity
	// M is the up/down velocity - why is this called "M"?
	L, M float64
}

func NewVel(l, m float64) Vel {
	return Vel{l, m}
}
