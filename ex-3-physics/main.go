package main

import (
	"log"

	ebiten "github.com/hajimehoshi/ebiten/v2"
	"github.com/meeech/ex-3/component"
	"github.com/meeech/ex-3/entity"
	"github.com/meeech/ex-3/system"
	"github.com/sedyh/mizu/pkg/engine"
	"github.com/solarlune/resolv"
)

func main() {
	ebiten.SetWindowResizable(true)
	ebiten.SetFPSMode(ebiten.FPSModeVsyncOffMaximum)
	ebiten.SetWindowSize(480, 640)
	if err := ebiten.RunGame(engine.NewGame(&Game{})); err != nil {
		log.Fatal(err)
	}
}

var space *resolv.Space

type Game struct{}

// Main scene, you can use that for settings or main menu
func (g *Game) Setup(w engine.World) {
	bounds := w.Bounds()

	space = resolv.NewSpace(bounds.Dx(), bounds.Dy(), 10, 10)
	w.AddComponents(
		component.Dim{},
		component.Physics{},
		component.Pos{},
		component.Rad{},
		component.Renderer{},
		component.Vel{},
		component.Debug{},
	)

	for i := 0; i < 8; i++ {
		yOffset := i * 80
		xOffset := 20
		if i%2 == 0 {
			xOffset = -xOffset
		}
		for j := 1; j < 6; j++ {
			x, y := float64((j*80)+xOffset), float64(50+yOffset)
			w.AddEntities(entity.NewPeg(space, x, y, 15.0))
		}
	}

	w.AddEntities(entity.NewDisc(space, float64(bounds.Dx())/3, 10, 10))

	w.AddSystems(
		&system.Gravity{},
		&system.Velocity{},
		system.NewSpace(space),
		&system.Render{},
		system.NewDebug(false),
	)
}
