package system

import (
	"github.com/meeech/ex-3/component"
	"github.com/sedyh/mizu/pkg/engine"
)

const gravity = .098

type Gravity struct {
	*component.Vel
}

func NewGravity() *Gravity {
	return &Gravity{}
}

func (g *Gravity) Update(_ engine.World) {
	// Increase vertical speed.
	g.Vel.M += gravity
}
