package system

import (
	"math/rand"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/meeech/ex-3/component"
	"github.com/sedyh/mizu/pkg/engine"
	"github.com/solarlune/resolv"
)

type Space struct {
	space *resolv.Space
}

func NewSpace(space *resolv.Space) *Space {
	return &Space{
		space: space,
	}
}

func (s *Space) Draw(w engine.World, screen *ebiten.Image) {
	// log.Println("system.Space.Draw")
}

func (s *Space) Update(w engine.World) {
	// anything with radius
	w.View(
		component.Pos{},
		component.Physics{},
		component.Rad{},
	).Each(func(e engine.Entity) {
		var pos *component.Pos
		var vel *component.Vel
		var phys *component.Physics
		var rad *component.Rad
		e.Get(&pos, &vel, &phys, &rad)

		phys.Object.X = pos.X - rad.Value
		phys.Object.Y = pos.Y - rad.Value
		phys.Object.Update()
	})

	w.View(component.Pos{}, component.Vel{}).Each(func(e engine.Entity) {
		var pos *component.Pos
		var vel *component.Vel
		e.Get(&pos, &vel)
		if pos.Y > float64(w.Bounds().Dy()) {
			pos.X = RangeFloat(0, float64(w.Bounds().Dx()))
			pos.Y = 0
		}

		if pos.X > float64(w.Bounds().Dx()) || pos.X < 0 {
			pos.X = RangeFloat(0, float64(w.Bounds().Dx()))
			pos.Y = 0
			vel.L = 0
			vel.M = 0
		}
	})
}

func RangeFloat(min, max float64) float64 {
	return min + rand.Float64()*(max-min)
}
