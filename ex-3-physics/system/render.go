package system

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/meeech/ex-3/component"
	"github.com/sedyh/mizu/pkg/engine"
	"golang.org/x/image/colornames"
)

func NewRender() *Render {
	return &Render{}
}

// When you need many sets of components
// in one system, you can use the views
// To expand, what is it saying, usually you 'bind' a system to a component
type Render struct{}

// Render one frame
func (r *Render) Draw(w engine.World, screen *ebiten.Image) {
	// choose the right entities yourself
	screen.Fill(colornames.Lightblue)

	ents := w.View(component.Renderer{})
	ents.Each(func(e engine.Entity) {
		// for now, assume anything with a renderer has a position
		var dim *component.Dim
		var pos *component.Pos
		var rad *component.Rad
		var renderer *component.Renderer
		e.Get(&pos, &renderer, &rad, &dim)
		// draw the component
		renderer.Draw(screen, pos, rad, dim)
	})
	// Query this view for all entities with a Pos & Renderer?
	// Will it grab anything with a Pos but not a Rad?
	// Correct - it will only grab things matching fully
	// This catches the Ball AND Wall
	// Anything caught by this should have a draw method
	// walls := w.View(component.Pos{}, component.Renderer{})
	// walls.Each(func(entity engine.Entity) {
	// 	log.Printf("system.Render.Draw: %+v", entity.ID())

	// 	var pos *component.Pos
	// 	var renderer *component.Renderer
	// 	entity.Get(&pos, &renderer)
	// 	renderer.Draw(screen, pos)
	// })

	// resolv
	// dx := 2.0
	// if collision := playerObj.Check(dx, 0); collision != nil {
	// 	dx = collision.ContactWithObject(collision.Objects[0]).X()
	// }
	// playerObj.X += dx
}
