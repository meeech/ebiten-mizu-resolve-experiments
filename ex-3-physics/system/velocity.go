package system

import (
	"log"
	"math"

	"github.com/meeech/ex-3/component"
	"github.com/sedyh/mizu/pkg/engine"
)

type Velocity struct{}

// We need to handle some different cases here for collisions.
// So now, we will manually get the entitys / components we need.
func (v *Velocity) Update(w engine.World) {
	// Get anything with a pos, vel, and physical object
	log.Println("system.Velocity.Update()")
	w.View(
		component.Pos{},
		component.Vel{},
		component.Physics{},
	).Each(func(e engine.Entity) {
		var pos *component.Pos
		var vel *component.Vel
		var phys *component.Physics
		e.Get(&pos, &vel, &phys)
		dx, dy := 0.0, 0.0
		// ahhh!!! because i'm clamping, but not setting?
		if collision := phys.Object.Check(vel.L, vel.M); collision != nil {
			log.Printf("COLLLLINSIOSNS!!!!***** %+v ", collision)
			// I think to do a bounce 'properly' need to use the collision
			// x/y from center, turn it into an angle, and then use that to
			// calculate the bounce angle > velocity.
			collidedWith := collision.Objects[0]
			colVec := collision.ContactWithObject(collidedWith)
			// Is our object to the left or right of the collidedWith object?
			if collidedWith.X > phys.Object.X {
				// We are to the right, so we need to bounce left
				vel.L += -0.5
			} else {
				// We are to the left, so we need to bounce right
				vel.L += 0.5
			}
			// Is our object above or below the collidedWith object?
			if collidedWith.Y > phys.Object.Y {
				// We are above, so we need to bounce down
				vel.M = -vel.M * 0.5
			} else {
				// We are below, so we need to bounce up
				vel.M = -vel.M
			}

			dx, dy = colVec.X(), colVec.Y()
			// col := collision.ContactWithObject(collision.Objects[0])
			// unit := col.Unit()
			// log.Printf("UNIT: %+v", unit)
			// // x axis
			// dx := collision.ContactWithObject(collision.Objects[0]).X()
			// log.Println(dx)
			// if dx == 0 {
			// 	dx = 1
			// }

			// vel.L = dx
			// vel.M = collision.ContactWithObject(collision.Objects[0]).Y() * 2
		}
		log.Printf("DX: %+v, DY: %+v", dx, dy)
		// Clamp vels to -2/2
		vel.L = math.Max(-2.0, math.Min(2.0, vel.L))
		vel.M = math.Max(-2.0, math.Min(5.0, vel.M))
		pos.X += vel.L
		pos.Y += vel.M
		log.Printf("system.Velocity.Update %+v", vel)
	})
}
