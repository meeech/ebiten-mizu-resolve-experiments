package main

import (
	"log"

	ebiten "github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/sedyh/mizu/pkg/engine"
	"golang.org/x/image/colornames"
)

func main() {
	ebiten.SetWindowResizable(true)
	ebiten.SetFPSMode(ebiten.FPSModeVsyncOffMaximum)

	if err := ebiten.RunGame(engine.NewGame(&Game{})); err != nil {
		log.Fatal(err)
	}
}

// When you need many sets of components
// in one system, you can use the views
// To expand, what is it saying, usually you 'bind' a system to a component
type Render struct{}

// Render one frame
func (r *Render) Draw(w engine.World, screen *ebiten.Image) {
	// choose the right entities yourself
	screen.Fill(colornames.Lightblue)

	// Query this view for all entities with a Pos & Rad?
	// Will it grab anything with a Pos but not a Rad?
	// Correct - it will only grab things matching fully
	view := w.View(Pos{}, Rad{})
	view.Each(func(entity engine.Entity) {
		var pos *Pos
		var rad *Rad
		log.Printf("%+v", entity)
		entity.Get(&pos, &rad)
		ebitenutil.DrawRect(
			screen,
			pos.X-rad.Value/2, pos.Y-rad.Value/2,
			rad.Value, rad.Value,
			colornames.Green,
		)
	})

	// This catches the Ball AND Wall
	walls := w.View(Pos{})
	walls.Each(func(entity engine.Entity) {
		log.Printf("%+v", entity)

		var pos *Pos
		entity.Get(&pos)
		ebitenutil.DrawRect(
			screen, pos.X, pos.Y, 10, 100, colornames.Mediumorchid,
		)
	})
}

type Game struct{}

// Main scene, you can use that for settings or main menu
func (g *Game) Setup(w engine.World) {
	bounds := w.Bounds()
	log.Println("Setup")
	w.AddComponents(Pos{}, Rad{}, Vel{})
	w.AddEntities(
		&Ball{
			Pos{float64(bounds.Dx() / 2), float64(bounds.Dy()) / 2},
			Rad{30},
			Vel{1, 0},
		},
		&Wall{Pos{40, 20}},
	)
	w.AddSystems(&Render{}, &Velocity{}, &Gravity{force: -1.0})
}

// Not sure where layout goes to???
func (g *Game) Layout(w, h int) (int, int) {
	return w, h
}

// Your game object
type Wall struct {
	Pos
}

// Your game object
type Ball struct {
	Pos // Ball position
	Rad // Ball radius
	Vel
}

type Velocity struct {
	*Pos
	*Vel
}

func (v *Velocity) Update(w engine.World) {
	log.Println("Update")
	v.Pos.X += v.Vel.L
	v.Pos.Y += v.Vel.M
}

type Gravity struct {
	*Vel
	*Pos
	force float64
}

func (g *Gravity) Update(w engine.World) {
	g.Pos.Y -= g.force
}

// Position for any entity, if it needs
type Pos struct {
	X, Y float64 // Just a 2D point
}

// Radius for any entity, if it needs
type Rad struct {
	Value float64 // Width value
}

// Position for any entity, if it needs
type Vel struct {
	L, M float64 // Just a 2D point
}
